using ProductService.Models;
using System;
using System.Data.Entity.Migrations;

namespace ProductService.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<ProductService.Models.ProductServiceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ProductServiceContext context)
        {
            context.Products.AddOrUpdate(
                x => x.Id,
                new Product()
                {
                    Id = 1,
                    Name = "������� ��������� MXP i3-8100, 8��, 240��",
                    Description = "������� ��������� MicroXperts Intel� Core� i3-8100, 8 ��, SSD 240 ��, Intel� HD Graphics, no DVD, no OS",
                    CreateDate = DateTime.Now,
                    PlaceStorage = 1,
                    Reserved = false
                },
                new Product()
                {
                    Id = 2,
                    Name = "���������� ��������� Apple iPad New Wi-Fi + Cellular 128GB Silver, MR732RU/A",
                    Description = "������� Apple iPad New Wi-Fi + Cellular 128GB, MR732RU/A, 9.7\" (2048x1536) Retina, Apple A10, RAM 2GB, 128GB, WiFi, BT, 3G, 4G(LTE), GPS, iOS11, Silver, �����������",
                    CreateDate = DateTime.Now - TimeSpan.FromDays(1),
                    PlaceStorage = 2,
                    Reserved = true
                },
                new Product()
                {
                    Id = 3,
                    Name = "��������� HP t530 GX-215JJ, 4GB, Win10IoT Enterprise, 2DH80AA",
                    Description = "��������� HP t530, 2DH80AA, AMD GX-215JJ, 4GB, 32GB SSD, Wi-Fi, BT, Win10IoT Enterprise, ����������, ����",
                    CreateDate = DateTime.Now,
                    PlaceStorage = 1,
                    Reserved = false
                },
                new Product()
                {
                    Id = 4,
                    Name = "�������� ������� HP Color LaserJet Professional CP5225n",
                    Description = "�������� ������� HP Color LaserJet Professional CP5225n, �������, A3, 600x600 �/�, 20 ���/���, �������, USB 2.0 (CE711A)",
                    CreateDate = DateTime.Now - TimeSpan.FromDays(2),
                    PlaceStorage = 3,
                    Reserved = false
                },
                new Product()
                {
                    Id = 5,
                    Name = "�������� ������� Kyocera FS-9530DN",
                    Description = "�������� ������� Kyocera FS-9530DN, �3, 600x1200 �/�, 51 ���/���, �������, USB 2.0",
                    CreateDate = DateTime.Now - TimeSpan.FromDays(3),
                    PlaceStorage = 3,
                    Reserved = false
                });
        }
    }
}