﻿
ko.bindingHandlers["datepicker"] = {
	init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
		$(element)
			.datepicker({ format: "dd.mm.yyyy", language: 'ru' })
			.on('changeDate', function (e) {
				var prop = valueAccessor();

				if (ko.isObservable(prop)) {
					prop(e.date);
				}
			})
			.on('change', function (e) {
				if (!$(element).val()) {
					var prop = valueAccessor();
					if (ko.isObservable(prop)) {
						prop(null);
					}
				}
			});
	},
	update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
		var value = ko.unwrap(valueAccessor());

		if (value && typeof value === "string") {
			value = new Date(value);
		}

		if (value) {
			$(element).datepicker("update", value);
		}
	}
};

ko.bindingHandlers['modal'] = {
	init: function (element, valueAccessor, allBindingsAccessor) {
		var allBindings = allBindingsAccessor();
		var $element = $(element);
		$element.addClass('hide modal');

		if (allBindings.modalOptions && allBindings.modalOptions.beforeClose) {
			$element.on('hide', function () {
				var value = ko.utils.unwrapObservable(valueAccessor());
				return allBindings.modalOptions.beforeClose(value);
			});
		}
	},
	update: function (element, valueAccessor) {
		var value = ko.utils.unwrapObservable(valueAccessor());
		if (value) {
			$(element).removeClass('hide').modal('show');
		} else {
			$(element).modal('hide');
		}
	}
};

var ProductDetailViewModel = function (id, name, description, createDate, placeStorage, reserved) {
	var self = this;
	self.Id = ko.observable(ko.utils.unwrapObservable(id));
	self.Name = ko.observable(ko.utils.unwrapObservable(name));
	self.Description = ko.observable(ko.utils.unwrapObservable(description));
	self.CreateDate = ko.observable(ko.utils.unwrapObservable(createDate));
	self.PlaceStorage = ko.observable(ko.utils.unwrapObservable(placeStorage));
	self.Reserved = ko.observable(ko.utils.unwrapObservable(reserved));
}

var ViewModel = function () {
	var self = this;
	
	self.Id = ko.observable();
	self.Name = ko.observable();
	self.Description = ko.observable();
	self.CreateDate = ko.observable();
	self.PlaceStorage = ko.observable();
	self.Reserved = ko.observable();

	self.product = ko.observable();
	self.error = ko.observable();	
	self.products = ko.observableArray();
	
	var productsUri = '/api/Products/';

	function ajaxHelper(uri, method, data) {
		self.error('');
		return $.ajax({
			type: method,
			url: uri,
			dataType: 'json',
			contentType: 'application/json',
			data: data ? JSON.stringify(data) : null
		}).fail(function (jqXHR, textStatus, errorThrown) {
			self.error(errorThrown);
		});
	}

	function getAllProducts() {
		ajaxHelper(productsUri, 'GET').done(function (data) {
			self.products(data);
		});
	}

	self.editProduct = function (product) {
		self.product(new ProductDetailViewModel(product.Id, product.Name, product.Description, product.CreateDate, product.PlaceStorage, product.Reserved));
	}

	self.addProduct = function () {
		self.product(new ProductDetailViewModel(undefined, '', '', new Date(), 0, false));
	}

	self.saveChanges = function () {
		var updatedProduct = ko.utils.unwrapObservable(self.product());

		var product = {
			Id: ko.utils.unwrapObservable(updatedProduct.Id),
			Name: ko.utils.unwrapObservable(updatedProduct.Name),
			Description: ko.utils.unwrapObservable(updatedProduct.Description),
			CreateDate: ko.utils.unwrapObservable(updatedProduct.CreateDate),
			PlaceStorage: ko.utils.unwrapObservable(updatedProduct.PlaceStorage),
			Reserved: ko.utils.unwrapObservable(updatedProduct.Reserved),
		};

		if (product.Id === undefined) {
			ajaxHelper(productsUri, 'POST', product).done(function (item) {
				getAllProducts();
			});
		}
		else {
			ajaxHelper(productsUri + product.Id, 'PUT', product).done(function () {
				getAllProducts();
			});
		}

		self.product(undefined);
	}

	self.deleteProduct = function (product) {
		if (confirm(`Вы действительно хотите удалить запись "${product.Name}"?`)) {
			ajaxHelper(productsUri + product.Id, 'DELETE').done(function () {
				getAllProducts();
			});
		}
	}

	getAllProducts();
}

ko.applyBindings(new ViewModel());