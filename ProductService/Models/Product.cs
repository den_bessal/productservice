﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductService.Models
{
    [Table("products")]
    public class Product
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [MaxLength(512)]
        [Column("name")]
        public string Name { get; set; }

        [MaxLength(1024)]
        [Column("description")]
        public string Description { get; set; }

        [Column("create_date")]
        public DateTime CreateDate { get; set; }

        [Column("place_storage")]
        public int PlaceStorage { get; set; }

        [Column("reserved")]
        public bool Reserved { get; set; }
    }
}