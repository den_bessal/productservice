#ProductService#
##Тестовое задание №4##
Клонируйте репозиторий или скачайте исходный код: [Исходники](https://bitbucket.org/den_bessal/productservice/get/270b37e7baa0.zip)

Комплект файлов для развертывания на сервере: [ProductService.zip](https://bitbucket.org/den_bessal/productservice/downloads/ProductService.zip)

Инструкция по развертыванию: [Инструкция по развертыванию.pdf](https://bitbucket.org/den_bessal/productservice/downloads/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D1%8F%20%D0%BF%D0%BE%20%D1%80%D0%B0%D0%B7%D0%B2%D0%B5%D1%80%D1%82%D1%8B%D0%B2%D0%B0%D0%BD%D0%B8%D1%8E.pdf)